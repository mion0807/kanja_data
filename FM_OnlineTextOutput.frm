VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FM_OnlineTextOutput 
   Caption         =   "目視確認画面"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6915
   OleObjectBlob   =   "FM_OnlineTextOutput.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "FM_OnlineTextOutput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private lStartRow       As Long     '管理シート対象データ部開始行
Private lEndRow         As Long     '管理シート対象データ部終了行
Private strDriveName    As String   '読込ドライブ名

Private lCntTan         As Long     '単記票の数
Private lCntRen         As Long     '連記票の数
Private lCntInT         As Long     'テキスト読込の数
Private lCntInD         As Long     'DPC読込の数
Private lCntInR         As Long     'レセプト読込の数

'*************************************************************************
'関数名　：frmInit
'概要　　：初期処理
'引数　　：lSRow As Long    '管理シート対象データ開始行
'          lERow As Long    '管理シート対象データ終了行
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub frmInit(lSRow As Long, lERow As Long, strDName As String)

On Error GoTo ErrProc

    Dim strMsg As String
    
    'ローカル変数に値をセット
    lStartRow = lSRow
    lEndRow = lERow
    strDriveName = strDName
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：frmInit " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    strMsg = "テキスト出力処理を実行します。" & vbCrLf & "宜しいですか？"
    If MsgBox(strMsg, vbYesNo, "テキストファイル　出力確認") = vbYes Then
        Call btnOK_Click
    Else
        Call btnCancel_Click
    End If
    Exit Sub
    
End Sub

'オンライン回答送信用に使用
Public Sub btnTxtOnlineRoute()
 Call outTextDataMain
End Sub


'*************************************************************************
'関数名　：btnOK_Click
'概要　　：OKボタン押下処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnOK_Click()

Dim strMsg          As String

On Error GoTo ErrProc
            
        '実行中画面表示
        FM_Now.Show (vbModeless)
        DoEvents
            
        'ラベル目視に○を付与
        Call subLableMark(co_Maru)
        
        'テキスト出力処理
        outTextDataMain
        
        '実行中画面閉じる
        Unload FM_Now
        
        '完了メッセージ
        strMsg = "テキスト出力処理が完了しました"
        MsgBox strMsg, vbOKOnly, "完了メッセージ"
        
        '画面を閉じる
        Unload Me
    'End If
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnOK_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub
    
End Sub

'*************************************************************************
'関数名　：btnNG_Click
'概要　　：NGボタン押下処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnNG_Click()

Dim strMsg          As String

On Error GoTo ErrProc
    
    '確認メッセージ
    strMsg = "NG処理を実行します。"
    If MsgBox(strMsg, vbOKCancel, "テキストファイル　NG確認") = vbOK Then
        'ラベル目視に×を付与
        Call subLableMark(co_Batsu)
        
        '完了メッセージ
        strMsg = "NG処理が完了しました"
        MsgBox strMsg, vbOKOnly, "完了メッセージ"
        
        '画面を閉じる
        Unload Me
    End If
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnNG_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnCancel_Click
'概要　　：キャンセルボタン押下処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnCancel_Click()

Dim strMsg          As String
    
On Error GoTo ErrProc
    
    '確認メッセージ
    strMsg = "テキスト出力をキャンセルしました。"
    If MsgBox(strMsg, vbOKOnly, "テキスト出力 キャンセル確認") = vbOK Then
        '画面を閉じる
        Unload Me
    End If
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnCancel_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：subLableMark
'概要　　：管理シートのラベル目視に印をつける
'引数　　：strMark as String    付与する印
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub subLableMark(strMark As String)

'ツールファイル用変数
Dim orgWB           As Workbook             'ツールファイル
Dim lRow            As Long                 '管理シートデータ部処理カレント行

On Error GoTo ErrProc
    
    'WorkBook変数の指定
    Set orgWB = ActiveWorkbook  'ツールのエクセルオブジェクト指定
    
    'ラベル目視に印をつける
    For lRow = lStartRow To lEndRow
        orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERLmCol) = strMark
    Next
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：subLableMark " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    Set orgWB = Nothing
    Exit Sub

End Sub

Private Sub btnTxtOutOnline_Click()
 Call outTextDataMain
End Sub

'*************************************************************************
'関数名　：outTextDataMain
'概要　　：テキストデータ出力処理(メイン関数)
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub outTextDataMain()

'ツールファイル用変数
Dim orgWB           As Workbook             'ツールファイル
Dim lRow            As Long                 '管理シートデータ部処理カレント行

'入力元ファイル用変数
Dim tmpWB           As Workbook             '各エクセルファイル

'テキストファイル用変数
Dim adoStrText      As Object               'テキストストリーム
Dim strTextFileSub  As String               'テキストファイル名(各調査で固有部分)
Dim PrefectureCode  As String
Dim ShisetsuNo      As String

On Error GoTo ErrProc

    '非表示にセット(メッセージ、画面)
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    
    'WorkBook変数の指定
    Set orgWB = ActiveWorkbook  'ツールのエクセルオブジェクト指定
            
    '初期化
    g_Dlm = ""
    
    '項目間の区切り文字
    #If Debug_Flg = 1 Then
        g_Dlm = "|"
    #End If
    
    '管理シートを一件ずつ処理する
    With orgWB.Sheets(co_KanriSht)
        For lRow = lStartRow To lEndRow
          
            If InStrRev(.Cells(lRow, co_KCDFnCol), ".xlsm") Then
                
                'テキストファイルの作成
                Set adoStrText = CreateObject("ADODB.Stream")
                With adoStrText
                    .Open
                    .Type = co_adTypeText       'テキストタイプ
                    .Charset = co_Chara_EUC     '文字コード(EUC-JP)
                    .LineSeparator = co_LF      '改行コード(LF)
                End With
                
                'エクセルファイルを開く
                Application.EnableEvents = False    'マクロ無効
                Workbooks.Open FileName:=.Cells(lRow, co_KCDFnCol), ReadOnly:=True
                Application.EnableEvents = True     'マクロ有効
                Set tmpWB = ActiveWorkbook
    
                PrefectureCode = .Cells(lRow, co_KCDTdCol)
                ShisetsuNo = Mid(.Cells(lRow, co_KCDSsCol), 1, 1) & Mid(.Cells(lRow, co_KCDSsCol), 3, 3)
                
                'テキスト出力処理
                Select Case .Cells(lRow, co_KISCcCol)
                
                    '病院入院（奇数）票
                    Case co_HKN
                        Call makeTextHKN(tmpWB.Sheets(1), adoStrText)   'テキスト出力
                        strTextFileSub = co_HKN & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt            'テキストファイル名
                        
                    '病院外来（奇数）票
                    Case co_HKG
                        Call makeTextHKG(tmpWB.Sheets(1), adoStrText)   'テキスト出力
                        strTextFileSub = co_HKG & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt            'テキストファイル名
                        
                    '病院入院（偶数）票
                    Case co_HGN
                        Call makeTextHGN(tmpWB.Sheets(1), adoStrText)   'テキスト出力
                        strTextFileSub = co_HGN & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt            'テキストファイル名
                        .Cells(lRow, co_KSDRyCol) = lCntInR
                        
                    '病院外来（偶数）票
                    Case co_HGG
                        Call makeTextHGG(tmpWB.Sheets(1), adoStrText)   'テキスト出力
                        strTextFileSub = co_HGG & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt            'テキストファイル名
                        .Cells(lRow, co_KSDRyCol) = lCntInR
                        
                    '病院退院票
                    Case co_HT
                        Call makeTextHT(tmpWB.Sheets(1), adoStrText)    'テキスト出力
                        strTextFileSub = co_HT & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt             'テキストファイル名
                        .Cells(lRow, co_KSDDyCol) = lCntInD             'DPC読取
                        
                    '一般診療所票（入院）
                    Case co_CN
                        Call makeTextCN(tmpWB.Sheets(1), adoStrText)    'テキスト出力
                        strTextFileSub = co_CN & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt             'テキストファイル名
                        
                    '一般診療所票（外来）
                    Case co_CG
                        Call makeTextCG(tmpWB.Sheets(1), adoStrText)    'テキスト出力
                        strTextFileSub = co_CG & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt             'テキストファイル名
                        
                    '一般診療所退院票
                    Case co_CT
                        Call makeTextCT(tmpWB.Sheets(1), adoStrText)    'テキスト出力
                        strTextFileSub = co_CT & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt             'テキストファイル名
                        
                    '歯科診療所票
                    Case co_D
                        Call makeTextD(tmpWB.Sheets(1), adoStrText)     'テキスト出力
                        strTextFileSub = co_D & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt              'テキストファイル名
                        
                End Select
    
                'エクセルファイルを閉じる
                tmpWB.Close SaveChanges:=False
                        
                'テキストファイルの保存
                adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & strTextFileSub, co_adSaveCreateOverWrite

                adoStrText.Close
                Set adoStrText = Nothing
                
                '管理シートにデータ件数をセット
                .Cells(lRow, co_KSDTnCol) = lCntTan '単記票
                .Cells(lRow, co_KSDRnCol) = lCntRen '連記票
                .Cells(lRow, co_KSDTyCol) = lCntInT 'テキスト読取

            ElseIf InStrRev(.Cells(lRow, co_KCDFnCol), ".txt") Then
                Dim classCode As String
                classCode = Left(.Cells(lRow, co_KCDSsCol), 1)
                If classCode = "H" Then
                    Call H_TextOut(orgWB, lRow)
                ElseIf classCode = "C" Then
                    Call C_TextOut(orgWB, lRow)
                ElseIf classCode = "D" Then
                    Call D_TextOut(orgWB, lRow)
                End If
                
                'lRow = lRow + 1
            End If
            
            '解放
            Set tmpWB = Nothing
        Next
    End With
    
    '終了処理
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：outTextDataMain " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    If Not (tmpWB Is Nothing) Then
        tmpWB.Close
    End If
    Set tmpWB = Nothing
    Set orgWB = Nothing
    If Not (adoStrText Is Nothing) Then
        adoStrText.Close
    End If
    Set adoStrText = Nothing
    
    '表示にセット(メッセージ、画面)
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextHKN
'概要　　：病院入院（奇数）票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextHKN(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 4, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 2), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 3), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            
            '入院年月日
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '受療の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 4
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 4
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '病床の種別
            lCol = lCol + 5
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '紹介の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
            
            '入院の状況
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            
            '管理シートの件数カウントアップ
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If
            
            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'テキスト読込
                lCntInTTmp = lCntInTTmp + 1
            End If
            
            'KDC 2017/07/26 仕様書と合わせる修正
            '回答手段
            'lCol = lCol + 1
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
             '調査票フラグ
            strTextData = strTextData & "2"
        
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
        
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextHKN " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextHKG
'概要　　：病院外来（奇数）票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextHKG(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText( _
                                            Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 4, co_Zen) & g_Dlm                                 '漢字モード
            
            '外来の種別
            lCol = lCol + 2
            If .Cells(lRow, lCol) <> "" Then
                strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '1〜2の場合
            Else
                strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '3〜6の場合
            End If
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '前回診療(訪問) 月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '前回診療(訪問) 日

            '受療の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 4
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 3
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '紹介の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
                        
            '管理シートの件数カウントアップ
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
        
            'KDC 2017/07/26 仕様書と合わせる修正
            '回答手段
            'lCol = lCol + 1
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
             '調査票フラグ
            strTextData = strTextData & "2"
            
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextHKG " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextHGN
'概要　　：病院入院（偶数）票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextHGN(dataWST As Worksheet, adoStrText As Object)

    Dim strTextData As String   'テキスト格納用
    Dim lRow        As Long     '処理中の行番号
    Dim lCol        As Long     '処理中の列番号

    Dim lCntTanTmp  As Long     '単記票の数
    Dim lCntRenTmp  As Long     '連記票の数
    Dim lCntInTTmp  As Long     'テキスト読込の数
    Dim lCntInRTmp  As Long     'レセプト読込の数

    Dim lLoop       As Long     'ループ変数
    Dim lCNo        As Long     '調査票番号
    Dim lCNoCnt     As Long     '調査票番号の繰返し数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    lCntInRTmp = 0
    lCNo = 1
    lCNoCnt = 1
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInR = lCntInRTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckColG) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            lCol = co_DstartCol
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            '20件を超えたらカウントアップ
            If lCNoCnt > co_CNoUp Then
                lCNo = lCNo + 1
                lCNoCnt = 1
            End If
            '調査票番号
            strTextData = strTextData & padText(CStr(lCNo), co_Zr, 3, co_Han) & g_Dlm
            
            '入院・外来
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
                        
            '管理シートの件数カウントアップ
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'テキスト
                lCntInTTmp = lCntInTTmp + 1
            End If
            
             'レセプト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'レセプト
                lCntInRTmp = lCntInRTmp + 1
            End If
            
             '調査票フラグ
            strTextData = strTextData & "2"
            
            'KDC 2017/07/26 仕様書と合わせる修正
            '回答手段
            'lCol = lCol + 1
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
        
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
            lCNoCnt = lCNoCnt + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInR = lCntInRTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextHGN " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextHGG
'概要　　：病院外来（偶数）票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextHGG(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数
Dim lCntInRTmp  As Long     'レセプト読込の数

Dim lLoop       As Long     'ループ変数
Dim lCNo        As Long     '調査票番号
Dim lCNoCnt     As Long     '調査票番号の繰返し数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    lCntInRTmp = 0
    lCNo = 1
    lCNoCnt = 1
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInR = lCntInRTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckColG) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            lCol = co_DstartCol
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            '20件を超えたらカウントアップ
            If lCNoCnt > co_CNoUp Then
                lCNo = lCNo + 1
                lCNoCnt = 1
            End If
            '調査票番号
            strTextData = strTextData & padText(CStr(lCNo), co_Zr, 3, co_Han) & g_Dlm
                        
            '入院・外来
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '管理シートの件数カウントアップ
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'テキスト
                lCntInTTmp = lCntInTTmp + 1
            End If
            
            'レセプト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'レセプト
                lCntInRTmp = lCntInRTmp + 1
            End If
            
             '調査票フラグ
            strTextData = strTextData & "2"
                
            '回答手段
            'lCol = lCol + 1
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
            lCNoCnt = lCNoCnt + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInR = lCntInRTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextHGG " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextHT
'概要　　：病院退院票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextHT(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数
Dim lCntInDTmp  As Long     'DPC読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    lCntInDTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInD = lCntInDTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 4, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 2), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 3), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            '郵便番号
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 7, co_Han) & g_Dlm       '郵便番号
            
            '過去の入院の有無
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '有無
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
                        
            '入院年月日
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
                        
            '退院日
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm       '日
            
            '受療の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
            'strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 3
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 4
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '病床の種別
            lCol = lCol + 5
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '入院前の場所
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm           '入院前の場所
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 2), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 4, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 3), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 4), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 5
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
            
            '手術の有無
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '有無
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            'strTextData = strTextData & padText(.Cells(lRow, lCol + 4), co_Zr, 1, co_Han) & g_Dlm   '手術名
            
            '転帰
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '退院後の行き先
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm           '退院後の行き先
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm       '入院前と同・別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 3), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 4, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 4), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 5), vbLf, StrConv(co_Sp, vbWide)), co_Sp, 8, co_Zen) & g_Dlm
            
            '管理シートの件数カウントアップ
            lCol = lCol + 6
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
        
            'DPCファイル読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                'DPCファイル読込カウンタ
                lCntInDTmp = lCntInDTmp + 1
            End If
            
            '回答手段
            'lCol = lCol + 1
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
             '調査票フラグ
            strTextData = strTextData & "2"
            
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
        
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    lCntInD = lCntInDTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextHT " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextCN
'概要　　：一般診療所票（入院）のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextCN(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText( _
                                            Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 4, co_Zen) & g_Dlm                                 '漢字モード
                        
            '入院・外来の種別等
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            strTextData = strTextData & "     " & g_Dlm                                             'スペース(5文字)
            
            '受療の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 4
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 3
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '紹介の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
            
            '病床の種別
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '入院の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            
            '管理シートの件数カウントアップ
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
            
            '回答手段
'            lCol = lCol + 1
            strTextData = strTextData & "1" & g_Dlm
            'オンライン対応したらこちらに変更
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
             '調査票フラグ
            strTextData = strTextData & "2"
            
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
        
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextCN " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextCG
'概要　　：一般診療所票（外来）のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextCG(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText( _
                                            Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 4, co_Zen) & g_Dlm                                 '漢字モード
                        
            '入院・外来の種別等
            lCol = lCol + 2
            strTextData = strTextData & "       " & g_Dlm                                           'スペース(7文字)
            If .Cells(lRow, lCol) <> "" Then
                strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '1〜2の場合
            Else
                strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '3〜6の場合
            End If
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '前回診療月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '前回診療日
            
            '受療の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 4
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 3
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '紹介の状況
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
            
            'スペース(2文字)
            strTextData = strTextData & "  " & g_Dlm
                        
            '管理シートの件数カウントアップ
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
            
            '回答手段
'            lCol = lCol + 1
            strTextData = strTextData & "1" & g_Dlm
            'オンライン対応したらこちらに変更
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
             '調査票フラグ
            strTextData = strTextData & "2"
            
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextCG " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextCT
'概要　　：一般診療所退院票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextCT(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText( _
                                            Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 4, co_Zen) & g_Dlm                                 '漢字モード
                        
            '過去の入院の有無
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '有無
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
                        
            '入院年月日
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
                        
            '退院日
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm       '日
            
            '受療の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '受療種別
            strTextData = strTextData & padText(Replace(.Cells(lRow, lCol + 1) _
                                                , vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 20, co_Zen) & g_Dlm                                '主傷病名(漢字モード)
'            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 1, co_Han) & g_Dlm   '肝疾患の状況
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 1, co_Han) & g_Dlm   '外傷の原因
            
            '副傷病名 なし〜副傷病名 その他の疾患
            lCol = lCol + 3
            For lLoop = 0 To 15
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 16
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 3
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '病床の種別
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '入院前の場所
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm
            
            '来院時の状況
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '通常・救急
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '診療時間
                        
            '手術の有無
            lCol = lCol + 2
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '有無
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
'            strTextData = strTextData & padText(.Cells(lRow, lCol + 4), co_Zr, 1, co_Han) & g_Dlm   '手術名
            
            '転帰
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '退院後の行き先
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm
            
            '管理シートの件数カウントアップ
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
        
            '回答手段
'            lCol = lCol + 1
            strTextData = strTextData & "1" & g_Dlm
            'オンライン対応したらこちらに変更
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '調査票フラグ
            strTextData = strTextData & "2"
            
            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextCT " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：makeTextD
'概要　　：歯科診療所票のテキスト出力処理
'引数　　：dataWST As Worksheet データ部シート
'          adoStrText As Object テキストストリーム
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub makeTextD(dataWST As Worksheet, adoStrText As Object)

Dim strTextData As String   'テキスト格納用
Dim lRow        As Long     '処理中の行番号
Dim lCol        As Long     '処理中の列番号

Dim lCntTanTmp  As Long     '単記票の数
Dim lCntRenTmp  As Long     '連記票の数
Dim lCntInTTmp  As Long     'テキスト読込の数

Dim lLoop       As Long     'ループ変数

On Error GoTo ErrProc
    
    '--------------------------------
    '初期化
    '--------------------------------
    strTextData = ""
    lRow = co_DstartRow
    lCol = co_DstartCol
    lCntTanTmp = 0
    lCntRenTmp = 0
    lCntInTTmp = 0
    
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    
    '--------------------------------
    'テキスト出力処理
    '--------------------------------
    With dataWST
        Do While .Cells(lRow, co_DcheckCol) <> ""
            '調査票種別
            strTextData = .Range(co_DCsCell) & g_Dlm
            
            '都道府県番号
            strTextData = strTextData & padText(.Range(co_DTdCell), co_Zr, 2, co_Han) & g_Dlm
            
            '施設番号
            strTextData = strTextData & padText(.Range(co_DSnCell), co_Zr, 3, co_Han) & g_Dlm
            
            '患者番号
            lCol = co_DstartCol
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 4, co_Han) & g_Dlm
            
            '性別
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
                        
            '出生年月日
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '元号
            strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 2, co_Han) & g_Dlm   '年
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '日
            
            '患者の住所
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '当院と同・別
            strTextData = strTextData & padText( _
                                            Replace(.Cells(lRow, lCol + 1), vbLf, StrConv(co_Sp, vbWide)) _
                                        , co_Sp, 4, co_Zen) & g_Dlm                                 '漢字モード
                        
            '入院・外来の種別等
            lCol = lCol + 2
            '外来の種別
            If .Cells(lRow, lCol) <> "" Then
                strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm       '1〜2の場合
            Else
                strTextData = strTextData & padText(.Cells(lRow, lCol + 1), co_Zr, 1, co_Han) & g_Dlm   '3〜6の場合
            End If
            strTextData = strTextData & padText(.Cells(lRow, lCol + 2), co_Zr, 2, co_Han) & g_Dlm   '前回診療月
            strTextData = strTextData & padText(.Cells(lRow, lCol + 3), co_Zr, 2, co_Han) & g_Dlm   '前回診療日
                        
            '傷病名
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 2, co_Han) & g_Dlm
            
            '診療費等支払方法
            '負担区分 自費〜負担区分 介護
            lCol = lCol + 1
            For lLoop = 0 To 2
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '�T (保険)
            lCol = lCol + 3
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '�U (公費) 感染症〜�U (公費) その他
            lCol = lCol + 1
            For lLoop = 0 To 3
                strTextData = strTextData & padText(.Cells(lRow, lCol + lLoop), co_Zr, 1, co_Han) & g_Dlm
            Next
            
            '管理シートの件数カウントアップ
            lCol = lCol + 4
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntTanTmp = lCntTanTmp + 1
            Else
                '連記票
                lCntRenTmp = lCntRenTmp + 1
            End If

            'テキスト読込
            lCol = lCol + 1
            strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            If .Cells(lRow, lCol) = co_On Then
                '単記票
                lCntInTTmp = lCntInTTmp + 1
            End If
        
            '回答手段
'            lCol = lCol + 1
            strTextData = strTextData & "1" & g_Dlm
            'オンライン対応したらこちらに変更
            'strTextData = strTextData & padText(.Cells(lRow, lCol), co_Zr, 1, co_Han) & g_Dlm
            
            '調査票フラグ
            strTextData = strTextData & "2"

            'テキストファイルに出力
            If strTextData <> "" Then
                adoStrText.WriteText strTextData, 1
            End If
            
            'カウントアップ
            lRow = lRow + 1
        Loop
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    'Private変数にセット
    lCntTan = lCntTanTmp
    lCntRen = lCntRenTmp
    lCntInT = lCntInTTmp
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：makeTextD " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

Private Sub UserForm_Click()

End Sub

Private Sub H_TextOut(orgWB As Workbook, lRow As Long)
    Dim intFF As Integer            ' FreeFile値
    Dim lngLOF As Long              ' LOF値
    Dim lngPOS As Long              ' 読み込み位置
    Dim HeaderBuf As String
    Dim BaseInfoBuf As String
    Dim NyuinKisuBuf As String
    Dim GairaiKisuBuf As String
    Dim NyuinGusuBuf As String
    Dim GairaiGusuBuf As String
    Dim TaiinBuf As String
    Dim NyuinKisuNum As Long
    Dim GairaiKisuNum As Long
    Dim NyuinGusuNum As Long
    Dim GairaiGusuNum As Long
    Dim TaiinNum As Long
    Dim NyuinKisuTankiNum As Long
    Dim NyuinKisuRenkiNum As Long
    Dim NyuinKisuTextNum As Long
    Dim GairaiKisuTankiNum As Long
    Dim GairaiKisuRenkiNum As Long
    Dim GairaiKisuTextNum As Long
    Dim NyuinGusuTextNum As Long
    Dim NyuinGusuReceNum As Long
    Dim GairaiGusuTextNum As Long
    Dim GairaiGusuReceNum As Long
    Dim TaiinTankiNum As Long
    Dim TaiinRenkiNum As Long
    Dim TaiinTextNum As Long
    Dim TaiinDPCNum As Long
    Dim Shubetsu As String
    Dim i As Integer
    Dim KeyItem As String
    Dim strTextData As String
    Dim StrFN As String
    Dim IntFlNo As Integer
    Dim lCNo        As Long     '調査票番号
    Dim lCNoCnt     As Long     '調査票番号の繰返し数
    Dim PrefectureCode As String
    Dim ShisetsuNo As String
    Dim adoStrText As Variant

    HeaderBuf = Space(85)
    BaseInfoBuf = Space(210)
    NyuinKisuBuf = Space(98)
    GairaiKisuBuf = Space(77)
    NyuinGusuBuf = Space(19)
    GairaiGusuBuf = Space(19)
    TaiinBuf = Space(167)
    Shubetsu = Space(1)

    StrFN = orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & "オンライン管理表" & Format(Now, "YYYYMMDDHHNN") & ".csv"
    IntFlNo = FreeFile
    Open StrFN For Output As #IntFlNo
    
    intFF = FreeFile
    
    ' 指定ファイルをOPEN(入力モード)
    Open Sheet2.Cells(lRow, co_KCDFnCol) For Binary As #intFF
    lngLOF = LOF(intFF)             ' LOF値(ファイルサイズ)取得
    lngPOS = 1                      ' 読み込み位置
    
    Do Until lngPOS > lngLOF
        Dim lRowNy
        Get #intFF, lngPOS, HeaderBuf
        lngPOS = lngPOS + 85
        
        Get #intFF, lngPOS, BaseInfoBuf
        lngPOS = lngPOS + 210
        
        NyuinKisuNum = 0
        GairaiKisuNum = 0
        NyuinGusuNum = 0
        GairaiGusuNum = 0
        TaiinNum = 0
        NyuinKisuTankiNum = 0
        NyuinKisuRenkiNum = 0
        NyuinKisuTextNum = 0
        GairaiKisuTankiNum = 0
        GairaiKisuRenkiNum = 0
        GairaiKisuTextNum = 0
        NyuinGusuTextNum = 0
        GairaiGusuTextNum = 0
        NyuinGusuReceNum = 0
        GairaiGusuReceNum = 0
        TaiinTankiNum = 0
        TaiinRenkiNum = 0
        TaiinTextNum = 0
        TaiinDPCNum = 0
    
        KeyItem = Trim(Mid(HeaderBuf, 21, 24))
        PrefectureCode = Mid(KeyItem, 14, 2)
        ShisetsuNo = Mid(KeyItem, 16, 4)
         
        '######################################################################################
        'テキストファイルの作成
        '入院奇数
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 700
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 99
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, NyuinKisuBuf
                lngPOS = lngPOS + LenB(StrConv(NyuinKisuBuf, vbFromUnicode))
                Debug.Print (i & ":[" & NyuinKisuBuf & "](サイズ:" & LenB(StrConv(NyuinKisuBuf, vbFromUnicode)) & ")")
            
                strTextData = Shubetsu & PrefectureCode & NyuinKisuBuf & "3"
            
                adoStrText.WriteText strTextData, 1
                
                NyuinKisuNum = NyuinKisuNum + 1
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    '単記票
                    NyuinKisuTankiNum = NyuinKisuTankiNum + 1
                Else
                    '連記票
                    NyuinKisuRenkiNum = NyuinKisuRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    NyuinKisuTextNum = NyuinKisuTextNum + 1
                End If
            End If

            'カウントアップ
'            lRow = lRow + 1
        Next
    
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_HKN & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
    
        '######################################################################################
        'テキストファイルの作成
        '外来奇数
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 1700
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 78
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, GairaiKisuBuf
                lngPOS = lngPOS + LenB(StrConv(GairaiKisuBuf, vbFromUnicode))
                Debug.Print (i & ":[" & GairaiKisuBuf & "](サイズ:" & LenB(StrConv(GairaiKisuBuf, vbFromUnicode)) & ")")
            
                strTextData = Shubetsu & PrefectureCode & GairaiKisuBuf & "3"
        
                adoStrText.WriteText strTextData, 1
                
                GairaiKisuNum = GairaiKisuNum + 1
            
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    '単記票
                    GairaiKisuTankiNum = GairaiKisuTankiNum + 1
                Else
                    '連記票
                    GairaiKisuRenkiNum = GairaiKisuRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    GairaiKisuTextNum = GairaiKisuTextNum + 1
                End If
            End If

            'カウントアップ
'            lRow = lRow + 1
        Next
    
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_HKG & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        
        adoStrText.Close
        Set adoStrText = Nothing
        
        '######################################################################################
        'テキストファイルの作成
        '入院偶数
        'H-GNonl
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        lCNo = 1
        lCNoCnt = 1

        For i = 1 To 1450
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 20
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, NyuinGusuBuf
                lngPOS = lngPOS + LenB(StrConv(NyuinGusuBuf, vbFromUnicode))
                Debug.Print (i & ":[" & NyuinGusuBuf & "](サイズ:" & LenB(StrConv(NyuinGusuBuf, vbFromUnicode)) & ")")
            
                '20件を超えたらカウントアップ
                If lCNoCnt > co_CNoUp Then
                    lCNo = lCNo + 1
                    lCNoCnt = 1
                End If

                'strTextData = Shubetsu & PrefectureCode & NyuinGusuBuf & "3"
                strTextData = Shubetsu & PrefectureCode & Left(NyuinGusuBuf, 3) & padText(CStr(lCNo), co_Zr, 3, co_Han) & Right(NyuinGusuBuf, Len(NyuinGusuBuf) - 7) & "3"
            
                adoStrText.WriteText strTextData, 1
                
                NyuinGusuNum = NyuinGusuNum + 1
            
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    NyuinGusuTextNum = NyuinGusuTextNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    NyuinGusuReceNum = NyuinGusuReceNum + 1
                End If
            End If
        
            'カウントアップ
'            lRow = lRow + 1
            lCNoCnt = lCNoCnt + 1
        
        Next
           
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_HGN & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        
        adoStrText.Close
        Set adoStrText = Nothing
        
        '######################################################################################
        'テキストファイルの作成
        '外来偶数
        'H-GGonl
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        lCNo = 1
        lCNoCnt = 1
        
        For i = 1 To 3300
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 20
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, GairaiGusuBuf
                lngPOS = lngPOS + LenB(StrConv(GairaiGusuBuf, vbFromUnicode))
                Debug.Print (i & ":[" & GairaiGusuBuf & "](サイズ:" & LenB(StrConv(GairaiGusuBuf, vbFromUnicode)) & ")")
            
                '20件を超えたらカウントアップ
                If lCNoCnt > co_CNoUp Then
                    lCNo = lCNo + 1
                    lCNoCnt = 1
                End If
                
                'strTextData = Shubetsu & PrefectureCode & GairaiGusuBuf & "3"
                strTextData = Shubetsu & PrefectureCode & Left(GairaiGusuBuf, 3) & padText(CStr(lCNo), co_Zr, 3, co_Han) & Right(GairaiGusuBuf, Len(GairaiGusuBuf) - 7) & "3"

                                    
                adoStrText.WriteText strTextData, 1
        
                GairaiGusuNum = GairaiGusuNum + 1
            
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    GairaiGusuTextNum = GairaiGusuTextNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    GairaiGusuReceNum = GairaiGusuReceNum + 1
                End If
            End If
            
            'カウントアップ
'            lRow = lRow + 1
            lCNoCnt = lCNoCnt + 1

        Next
                  
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_HGG & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
        '######################################################################################
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 2800
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 168
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, TaiinBuf
                lngPOS = lngPOS + LenB(StrConv(TaiinBuf, vbFromUnicode))
                Debug.Print (i & ":[" & TaiinBuf & "](サイズ:" & LenB(StrConv(TaiinBuf, vbFromUnicode)) & ")")
                
                strTextData = Shubetsu & PrefectureCode & TaiinBuf & "3"
            
                adoStrText.WriteText strTextData, 1
                
                TaiinNum = TaiinNum + 1
            
                
                If Mid(strTextData, Len(strTextData) - 2, 1) = co_On Then
                    '単記票
                    TaiinTankiNum = TaiinTankiNum + 1
                Else
                    '連記票
                    TaiinRenkiNum = TaiinRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    TaiinTextNum = TaiinTextNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    TaiinDPCNum = TaiinDPCNum + 1
                End If
            End If
            
            'カウントアップ
'            lRow = lRow + 1
        Next
            
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_HT & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
        
        lngPOS = lngPOS + 2 ' CRLF分進める
        
        ' サマリーの出力
        Print #IntFlNo, PrefectureCode & "," & ShisetsuNo & "," & NyuinKisuNum & "," & GairaiKisuNum & "," & NyuinGusuNum & "," & GairaiGusuNum & "," & TaiinNum _
         & "," & NyuinKisuTankiNum & "," & NyuinKisuRenkiNum & "," & NyuinKisuTextNum _
         & "," & GairaiKisuTankiNum & "," & GairaiKisuRenkiNum & "," & GairaiKisuTextNum _
         & "," & NyuinGusuTextNum & "," & NyuinGusuReceNum & "," & GairaiGusuTextNum & "," & GairaiGusuReceNum _
         & "," & TaiinTankiNum & "," & TaiinRenkiNum & "," & TaiinTextNum & "," & TaiinDPCNum
        
    Loop
    
    ' 指定ファイルをCLOSE
    Close #intFF
    Close #IntFlNo


End Sub


Private Sub C_TextOut(orgWB As Workbook, lRow As Long)
    Dim intFF As Integer            ' FreeFile値
    Dim lngLOF As Long              ' LOF値
    Dim lngPOS As Long              ' 読み込み位置
    Dim HeaderBuf As String
    Dim BaseInfoBuf As String
    Dim NyuinBuf As String
    Dim GairaiBuf As String
    Dim TaiinBuf As String
    Dim NyuinNum As Long
    Dim GairaiNum As Long
    Dim TaiinNum As Long
    Dim NyuinTankiNum As Long
    Dim NyuinRenkiNum As Long
    Dim NyuinTextNum As Long
    Dim GairaiTankiNum As Long
    Dim GairaiRenkiNum As Long
    Dim GairaiTextNum As Long
    Dim TaiinTankiNum As Long
    Dim TaiinRenkiNum As Long
    Dim TaiinTextNum As Long
    Dim Shubetsu As String
    Dim i As Integer
    Dim KeyItem As String
    Dim strTextData As String
    Dim StrFN As String
    Dim IntFlNo As Integer
    Dim lCNo        As Long     '調査票番号
    Dim lCNoCnt     As Long     '調査票番号の繰返し数
    Dim PrefectureCode As String
    Dim ShisetsuNo As String
    Dim adoStrText As Variant

    HeaderBuf = Space(85)
    BaseInfoBuf = Space(208)
    NyuinBuf = Space(87)
    GairaiBuf = Space(87)
    TaiinBuf = Space(100)
    Shubetsu = Space(1)

    StrFN = orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & "オンライン管理表" & Format(Now, "YYYYMMDDHHNN") & ".csv"
    IntFlNo = FreeFile
    Open StrFN For Output As #IntFlNo
    
    intFF = FreeFile
    
    ' 指定ファイルをOPEN(入力モード)
    Open Sheet2.Cells(lRow, co_KCDFnCol) For Binary As #intFF
    lngLOF = LOF(intFF)             ' LOF値(ファイルサイズ)取得
    lngPOS = 1                      ' 読み込み位置
    
    Do Until lngPOS > lngLOF
        Dim lRowNy
        Get #intFF, lngPOS, HeaderBuf
        lngPOS = lngPOS + 85
        
        Get #intFF, lngPOS, BaseInfoBuf
        lngPOS = lngPOS + 208
        
        NyuinNum = 0
        GairaiNum = 0
        TaiinNum = 0
        NyuinTankiNum = 0
        NyuinRenkiNum = 0
        NyuinTextNum = 0
        GairaiTankiNum = 0
        GairaiRenkiNum = 0
        GairaiTextNum = 0
        TaiinTankiNum = 0
        TaiinRenkiNum = 0
        TaiinTextNum = 0
    
        KeyItem = Trim(Mid(HeaderBuf, 21, 24))
        PrefectureCode = Mid(KeyItem, 14, 2)
        ShisetsuNo = Mid(KeyItem, 16, 4)
         
        '######################################################################################
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 50
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 88
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, NyuinBuf
                lngPOS = lngPOS + LenB(StrConv(NyuinBuf, vbFromUnicode))
                Debug.Print (i & ":[" & NyuinBuf & "](サイズ:" & LenB(StrConv(NyuinBuf, vbFromUnicode)) & ")")
            
                strTextData = Shubetsu & PrefectureCode & Left(NyuinBuf, 86) & "3"
            
                adoStrText.WriteText strTextData, 1
                
                NyuinNum = NyuinNum + 1
                
                If Mid(strTextData, Len(strTextData) - 2, 1) = co_On Then
                    '単記票
                    NyuinTankiNum = NyuinTankiNum + 1
                Else
                    '連記票
                    NyuinRenkiNum = NyuinRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    NyuinTextNum = NyuinTextNum + 1
                End If
            End If

            'カウントアップ
'            lRow = lRow + 1
        Next
    
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_CN & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
    
        '######################################################################################
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 1500
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 88
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, GairaiBuf
                lngPOS = lngPOS + LenB(StrConv(GairaiBuf, vbFromUnicode))
                Debug.Print (i & ":[" & GairaiBuf & "](サイズ:" & LenB(StrConv(GairaiBuf, vbFromUnicode)) & ")")
            
                strTextData = Shubetsu & PrefectureCode & Left(GairaiBuf, 86) & "3"
        
                adoStrText.WriteText strTextData, 1
                
                GairaiNum = GairaiNum + 1
            
                If Mid(strTextData, Len(strTextData) - 2, 1) = co_On Then
                    '単記票
                    GairaiTankiNum = GairaiTankiNum + 1
                Else
                    '連記票
                    GairaiRenkiNum = GairaiRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    GairaiTextNum = GairaiTextNum + 1
                End If
            End If

            'カウントアップ
'            lRow = lRow + 1
        Next
    
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_CG & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        
        adoStrText.Close
        Set adoStrText = Nothing
     
        '######################################################################################
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 300
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 101
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, TaiinBuf
                lngPOS = lngPOS + LenB(StrConv(TaiinBuf, vbFromUnicode))
                Debug.Print (i & ":[" & TaiinBuf & "](サイズ:" & LenB(StrConv(TaiinBuf, vbFromUnicode)) & ")")
                
                strTextData = Shubetsu & PrefectureCode & Left(TaiinBuf, 99) & "3"
            
                adoStrText.WriteText strTextData, 1
                
                TaiinNum = TaiinNum + 1
            
                
                If Mid(strTextData, Len(strTextData) - 2, 1) = co_On Then
                    '単記票
                    TaiinTankiNum = TaiinTankiNum + 1
                Else
                    '連記票
                    TaiinRenkiNum = TaiinRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    TaiinTextNum = TaiinTextNum + 1
                End If
             
            End If
            
            'カウントアップ
'            lRow = lRow + 1
        Next
            
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_CT & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
        
        lngPOS = lngPOS + 2 ' CRLF分進める
        
        ' サマリーの出力
        Print #IntFlNo, PrefectureCode & "," & ShisetsuNo & "," & NyuinNum & "," & GairaiNum & "," & TaiinNum _
         & "," & NyuinTankiNum & "," & NyuinRenkiNum & "," & NyuinTextNum _
         & "," & GairaiTankiNum & "," & GairaiRenkiNum & "," & GairaiTextNum _
         & "," & TaiinTankiNum & "," & TaiinRenkiNum & "," & TaiinTextNum
        
    Loop
    
    ' 指定ファイルをCLOSE
    Close #intFF
    Close #IntFlNo


End Sub


Private Sub D_TextOut(orgWB As Workbook, lRow As Long)
    Dim intFF As Integer            ' FreeFile値
    Dim lngLOF As Long              ' LOF値
    Dim lngPOS As Long              ' 読み込み位置
    Dim HeaderBuf As String
    Dim BaseInfoBuf As String
    Dim SikaBuf As String
    Dim SikaNum As Long
    Dim SikaTankiNum As Long
    Dim SikaRenkiNum As Long
    Dim SikaTextNum As Long
    Dim Shubetsu As String
    Dim i As Integer
    Dim KeyItem As String
    Dim strTextData As String
    Dim StrFN As String
    Dim IntFlNo As Integer
    Dim lCNo        As Long     '調査票番号
    Dim lCNoCnt     As Long     '調査票番号の繰返し数
    Dim PrefectureCode As String
    Dim ShisetsuNo As String
    Dim adoStrText As Variant

    HeaderBuf = Space(85)
    BaseInfoBuf = Space(205)
    SikaBuf = Space(38)
    Shubetsu = Space(1)

    StrFN = orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & "オンライン管理表" & Format(Now, "YYYYMMDDHHNN") & ".csv"
    IntFlNo = FreeFile
    Open StrFN For Output As #IntFlNo
    
    intFF = FreeFile
    
    ' 指定ファイルをOPEN(入力モード)
    Open Sheet2.Cells(lRow, co_KCDFnCol) For Binary As #intFF
    lngLOF = LOF(intFF)             ' LOF値(ファイルサイズ)取得
    lngPOS = 1                      ' 読み込み位置
    
    Do Until lngPOS > lngLOF
        Dim lRowNy
        Get #intFF, lngPOS, HeaderBuf
        lngPOS = lngPOS + 85
        
        Get #intFF, lngPOS, BaseInfoBuf
        lngPOS = lngPOS + 205
        
        SikaNum = 0
        SikaTankiNum = 0
        SikaRenkiNum = 0
        SikaTextNum = 0
    
        KeyItem = Trim(Mid(HeaderBuf, 21, 24))
        PrefectureCode = Mid(KeyItem, 14, 2)
        ShisetsuNo = Mid(KeyItem, 16, 4)
         
        '######################################################################################
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Open
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
        End With
        
        For i = 1 To 300
            Get #intFF, lngPOS, Shubetsu
            
            If Shubetsu = " " Then
                lngPOS = lngPOS + 39
            Else
                lngPOS = lngPOS + 1
                Get #intFF, lngPOS, SikaBuf
                lngPOS = lngPOS + LenB(StrConv(SikaBuf, vbFromUnicode))
                Debug.Print (i & ":[" & SikaBuf & "](サイズ:" & LenB(StrConv(SikaBuf, vbFromUnicode)) & ")")
            
                strTextData = Shubetsu & PrefectureCode & Left(SikaBuf, 37) & "3" '8/4入力情報
                                             '↑都道府県番号
                adoStrText.WriteText strTextData, 1
                
                SikaNum = SikaNum + 1
                
                If Mid(strTextData, Len(strTextData) - 1, 1) = co_On Then
                    '単記票
                    SikaTankiNum = SikaTankiNum + 1
                Else
                    '連記票
                    SikaRenkiNum = SikaRenkiNum + 1
                End If
                
                If Mid(strTextData, Len(strTextData), 1) = co_On Then
                    SikaTextNum = SikaTextNum + 1
                End If
            End If

            'カウントアップ
'            lRow = lRow + 1
        Next
    
        'テキストファイルの保存
        adoStrText.SaveToFile orgWB.Sheets(co_MainSht).Range(co_oCell) & "\" & PrefectureCode & ShisetsuNo & co_D & co_ONLINE & Format(Now, "YYYYMMDDHHNN") & co_Ext_Txt, co_adSaveCreateOverWrite
        adoStrText.Close
        Set adoStrText = Nothing
        
        lngPOS = lngPOS + 2 ' CRLF分進める
        
        ' サマリーの出力
        Print #IntFlNo, PrefectureCode & "," & ShisetsuNo & "," & SikaNum _
         & "," & SikaTankiNum & "," & SikaRenkiNum & "," & SikaTextNum
        
    Loop
    
    ' 指定ファイルをCLOSE
    Close #intFF
    Close #IntFlNo


End Sub

VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit



Private Sub btnTxtOutOnline_Click()

Dim strMsg      As String
Dim fso         As New FileSystemObject 'FileSystemObject

'ドライブ情報
Dim dCollect    As Object   'ドライブコレクション
Dim dMember     As Object   'ドライブメンバ

On Error GoTo ErrProc
    
    '--------------------------------
    '入力チェック
    '--------------------------------
    If checkTxtOutData = False Then
        GoTo FinProc
    End If

    '確認メッセージ
    strMsg = "テキスト出力処理を実行します。" & vbCrLf & "宜しいですか？"
    If MsgBox(strMsg, vbYesNo, "テキストファイル　出力確認") = vbYes Then
        'CD情報設定画面
        With FM_InOnlineData
            '初期化
            .cmbTodofuken.Value = "01"
'            .cmbKbn.Value = "C"
'            .txtShisetsu = "999"
            
            'セットフォーカス
            .cmbTodofuken.SetFocus
            
            '画面を開く
            .Show (vbModeless)
        End With
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnTxtOut_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    Set dCollect = Nothing
    Set dMember = Nothing
    Set fso = Nothing
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnTxtOutSansyo_Click
'概要　　：調査票テキストファイル　出力先フォルダ参照ボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnTxtOutSansyo_Click()

'フォルダ選択ダイアログを表示し、選択したPATHをセットする
Dim buf As String

On Error GoTo ErrProc

    buf = GetDirectory("調査票テキストファイルの出力先フォルダを選択してください", Me.Range(co_oCell))
    If buf = "" Then
        Exit Sub
    Else
        Me.Range(co_oCell) = buf
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnTxtOutSansyo_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnTxtOut_Click
'概要　　：テキスト出力ボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnTxtOut_Click()

Dim strMsg      As String
Dim fso         As New FileSystemObject 'FileSystemObject

'ドライブ情報
Dim dCollect    As Object   'ドライブコレクション
Dim dMember     As Object   'ドライブメンバ

On Error GoTo ErrProc
    
    '--------------------------------
    '入力チェック
    '--------------------------------
    If checkTxtOutData = False Then
        GoTo FinProc
    End If

    '確認メッセージ
    strMsg = "テキスト出力処理を実行します。" & vbCrLf & "宜しいですか？"
    If MsgBox(strMsg, vbYesNo, "テキストファイル　出力確認") = vbYes Then
        'CD情報設定画面
        With FM_InCDData
            '初期化
            .cmbTodofuken.Value = "01"
            .cmbKbn.Value = "H"
            .txtShisetsu = ""
            
            'セットフォーカス
            .cmbTodofuken.SetFocus
            
            '画面を開く
            .Show (vbModeless)
        End With
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnTxtOut_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    Set dCollect = Nothing
    Set dMember = Nothing
    Set fso = Nothing
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnMrgInSansyo_Click
'概要　　：調査票テキストファイル（マージ）　入力用フォルダ参照ボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnMrgInSansyo_Click()

'フォルダ選択ダイアログを表示し、選択したPATHをセットする
Dim buf As String
    
On Error GoTo ErrProc

    buf = GetDirectory("マージ元の調査票テキストファイルのフォルダを選択してください", Me.Range(co_m_iCell))
    If buf = "" Then
        Exit Sub
    Else
        Me.Range(co_m_iCell) = buf
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnMrgInSansyo_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnMrgOutSansyo_Click
'概要　　：調査票テキストファイル（マージ）　出力先フォルダ参照ボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnMrgOutSansyo_Click()

'フォルダ選択ダイアログを表示し、選択したPATHをセットする
Dim buf As String
    
On Error GoTo ErrProc

    buf = GetDirectory("マージした調査票テキストファイルの出力先フォルダを選択してください", Me.Range(co_m_oCell))
    If buf = "" Then
        Exit Sub
    Else
        Me.Range(co_m_oCell) = buf
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnMrgOutSansyo_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnTxtMarge_Click
'概要　　：テキストマージボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnTxtMarge_Click()

Dim strMsg  As String

On Error GoTo ErrProc
    
    '--------------------------------
    '入力チェック
    '--------------------------------
    If checkTxtMargeData = False Then
        GoTo FinProc
    End If

    '確認メッセージ
    strMsg = "テキストマージ処理を実行します。" & vbCrLf & "宜しいですか？"
    If MsgBox(strMsg, vbYesNo, "テキストファイル　マージ確認") = vbYes Then
        'マージ処理
        txtMargeExe
        
        '完了メッセージ
        strMsg = "テキストマージ処理が完了しました"
        MsgBox strMsg, vbOKOnly, "完了メッセージ"
        
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnTxtMarge_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：checkTxtOutData
'概要　　：入力チェック処理(テキスト出力時)
'引数　　：なし
'戻り値　：Boolean  True:入力チェックOK、False:入力チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Function checkTxtOutData() As Boolean

Dim fso As New FileSystemObject 'FileSystemObject

On Error GoTo ErrProc

    '--------------------------------
    '初期化
    '--------------------------------
    checkTxtOutData = False

    '--------------------------------
    '必須チェック
    '--------------------------------
    '出力先フォルダ
    If Me.Range(co_oCell) = "" Then
        'エラーとして処理を終了
        MsgBox "調査票テキストファイルの出力先フォルダが選択されていません。選択してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '--------------------------------
    '存在チェック
    '--------------------------------
    '出力先フォルダ
    If fso.FolderExists(Me.Range(co_oCell)) = False Then
        'エラーとして処理を終了
        MsgBox "調査票テキストファイルの出力先フォルダが存在しません。再度フォルダを指定してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    checkTxtOutData = True
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：checkTxtOutData " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:
    '解放
    Set fso = Nothing

End Function

'*************************************************************************
'関数名　：checkTxtMargeData
'概要　　：入力チェック処理(テキストマージ時)
'引数　　：なし
'戻り値　：Boolean  True:入力チェックOK、False:入力チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Function checkTxtMargeData() As Boolean

Dim fso As New FileSystemObject 'FileSystemObject

On Error GoTo ErrProc

    '--------------------------------
    '初期化
    '--------------------------------
    checkTxtMargeData = False

    '--------------------------------
    '必須チェック
    '--------------------------------
    '入力用フォルダ
    If Me.Range(co_m_iCell) = "" Then
        'エラーとして処理を終了
        MsgBox "マージファイルの入力用フォルダが選択されていません。選択してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '出力先フォルダ
    If Me.Range(co_m_oCell) = "" Then
        'エラーとして処理を終了
        MsgBox "マージファイルの出力先フォルダが選択されていません。選択してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '--------------------------------
    '存在チェック
    '--------------------------------
    '入力用フォルダ
    If fso.FolderExists(Me.Range(co_m_iCell)) = False Then
        'エラーとして処理を終了
        MsgBox "マージファイルの入力用フォルダが存在しません。再度フォルダを指定してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '出力先フォルダ
    If fso.FolderExists(Me.Range(co_m_oCell)) = False Then
        'エラーとして処理を終了
        MsgBox "マージファイルの出力先フォルダが存在しません。再度フォルダを指定してください。", vbOKOnly + vbExclamation
        GoTo FinProc
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    checkTxtMargeData = True
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：checkTxtMargeData " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:
    '解放
    Set fso = Nothing

End Function

'*************************************************************************
'関数名　：txtMargeExe
'概要　　：テキストマージ処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub txtMargeExe()

'ファイル検索系
Dim fso             As New FileSystemObject 'FileSystemObject
Dim sFolder         As String               '検索フォルダ名
Dim sFoundSubFolder As String               'サブフォルダ内のファイルを取得するか？ TRUE:全て取得 それ以外:フォルダのみ
Dim sFileList()     As String               '処理対象ファイルリスト
Dim sFileType       As String               '処理対象ファイル拡張子(セミコロン区切り)
Dim lLoop           As Long                 '処理対象ファイルのループ変数

'テキストファイル用変数
Dim adoStrMrgText   As Object               'テキストストリーム(マージファイル)
Dim strMrgTextFile  As String               'テキストファイル名(マージファイル)
Dim adoStrText      As Object               'テキストストリーム(一つずつ)

'その他処理変数
Dim strMsg          As String

On Error GoTo ErrProc

    '非表示にセット(メッセージ、画面)
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    
    'フォルダ配下(サブフォルダ含む)のxlsファイルを全て処理する
    sFolder = Me.Range(co_m_iCell)  '検索ルートフォルダ
    sFileType = "*" & co_Ext_Txt    '検索ファイル名(複数ある場合はセミコロンで区切る)
    sFoundSubFolder = "TRUE"        'サブフォルダは検索対象
    ReDim sFileList(0)
    
    '対象ファイルを配列に格納
    Call FileList(sFolder, fso, sFoundSubFolder, sFileType, sFileList)
    
    '処理対象ファイルが存在しない場合は処理中断
    If UBound(sFileList) = 0 Then
        'メッセージ
        strMsg = "処理対象ファイルが存在しないため処理を中断します。" & vbCrLf & "再度指定したフォルダをご確認下さい。"
        MsgBox strMsg, vbOKOnly, "処理中断"
        GoTo FinProc
    End If
    
    'マージテキストファイルの作成
    Set adoStrMrgText = CreateObject("ADODB.Stream")
    With adoStrMrgText
        .Open
        .Type = co_adTypeText       'テキストタイプ
        .Charset = co_Chara_EUC     '文字コード(EUC-JP)
        .LineSeparator = co_LF      '改行コード(LF)
    End With
    
    '対象ファイルを一つずつ処理
    For lLoop = 0 To UBound(sFileList) - 1
        'テキストファイルの作成
        Set adoStrText = CreateObject("ADODB.Stream")
        With adoStrText
            .Type = co_adTypeText       'テキストタイプ
            .Charset = co_Chara_EUC     '文字コード(EUC-JP)
            .LineSeparator = co_LF      '改行コード(LF)
            .Open
            .LoadFromFile (sFileList(lLoop))
        End With
    
        'テキストのマージ
        adoStrMrgText.WriteText adoStrText.ReadText(-1)
        adoStrText.Close
        Set adoStrText = Nothing
    Next
    
    'テキストファイルの保存
    strMrgTextFile = Me.Range(co_m_oCell) & "\" & Format(Now, "YYYYMMDDHHNNSS") & co_MrgTxt & co_Ext_Txt
    adoStrMrgText.SaveToFile strMrgTextFile, co_adSaveCreateOverWrite
    adoStrMrgText.Close
    Set adoStrMrgText = Nothing
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：txtMargeExe " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    If Not (adoStrText Is Nothing) Then
        adoStrText.Close
    End If
    Set adoStrText = Nothing
    If Not (adoStrMrgText Is Nothing) Then
        adoStrMrgText.Close
    End If
    Set adoStrMrgText = Nothing
    
    '表示にセット(メッセージ、画面)
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True

End Sub

Attribute VB_Name = "Develop"
Option Explicit
'このコンポーネントは開発用の関数のみを作成し、本番では削除するので各処理では使用しないこと

Public Enum ComponentType
    '標準モジュール
    vbaModule = 1
    'クラス モジュール
    vbaClass = 2
    'Microsoft Forms
    vbaForm = 3
    'ActiveX デザイナー
    vbaActiveXDesigner = 11
    'ドキュメント モジュール
    vbaDocument = 100
End Enum

Public Sub vbaComponentExport()
    Dim realSrcPath As String
    realSrcPath = ThisWorkbook.path & "\src"
    Call mkdir(realSrcPath)
    
    Dim vbaComponent As Object
    For Each vbaComponent In ThisWorkbook.VBProject.VBComponents
        'Debug.Print vbaComponent.Name
        
        Dim extention As String
        extention = ""
        Select Case vbaComponent.Type
            Case ComponentType.vbaModule
                '標準モジュール
                extention = ".bas"
            Case ComponentType.vbaClass
                'クラス モジュール
                extention = ".cls"
            Case ComponentType.vbaForm
                'Microsoft Forms
                extention = ".frm"
            Case ComponentType.vbaActiveXDesigner
                'ActiveX デザイナー
                extention = ".cls"
            Case ComponentType.vbaDocument
                'ドキュメント モジュール
                extention = ".cls"
        End Select
        
        If extention <> "" Then
            vbaComponent.Export realSrcPath & "\" & vbaComponent.name & extention
        End If
    Next
End Sub


'サーバー上に再帰的にフォルダを作成する
'@param path ローカルの絶対パス
Public Sub mkdir(ByVal path As String)
    Dim fso As Object
    Dim parent As String
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    '親パスを取得
    parent = fso.GetParentFolderName(path)
    
    '親フォルダの存在確認
    If Not fso.FolderExists(parent) Then
        Call mkdir(parent)
    End If
    
    If Not fso.FolderExists(path) Then
        '親フォルダがあり、作成予定のフォルダがない場合は作成する
         fso.CreateFolder path
    End If

End Sub


Attribute VB_Name = "MD_Const"
'*************************************************************************************
' グローバル定義モジュール
'*************************************************************************************

'*************************************************************************
'定数定義
'*************************************************************************
'ドライブ情報
Public Const co_CDType = 4      'ドライブタイプ：CD-ROM

'管理シート
Public Const co_Haifun = "-"    '管理シートのデータ初期値
Public Const co_Maru = "○"     '管理シートの○印
Public Const co_Batsu = "×"    '管理シートの×印

'マスタシート
Public Const co_MstTdfk = "1"   '都道府県マスタの区分
Public Const co_MstCysh = "2"   '調査票マスタの区分

'調査票コード
Public Const co_HKN = "H-KN"    '病院入院（奇数）票
Public Const co_HKG = "H-KG"    '病院外来（奇数）票
Public Const co_HGN = "H-GN"    '病院入院（偶数）票
Public Const co_HGG = "H-GG"    '病院外来（偶数）票
Public Const co_HT = "H-T"      '病院退院票
Public Const co_CN = "C-N"      '一般診療所票（入院）
Public Const co_CG = "C-G"      '一般診療所票（外来）
Public Const co_CT = "C-T"      '一般診療所退院票
Public Const co_D = "D"         '歯科診療所票
Public Const co_ONLINE = "onl"  'オンライン

'オンライン調査票ID
Public Const taishosha_H = "009990030775"   '病院票
Public Const taishosha_C = "009990030776"   '一般診療所
Public Const taishosha_D = "009990030777"   '歯科


'時間関係
Public Const co_LimitTime = 60  'CDドライブ読込待ち時間の制限(秒)

'調査票参照先のファイルパス
Public kaitoFilePath As Variant

'調査票参照先のファイルパス
Public textFileName As String

'データ出力区分
Public Const co_Han = 1         '半角
Public Const co_Zen = 2         '全角

'パディング用
Public Const co_Zr = "0"        'ゼロ
Public Const co_Sp = " "        'スペース

'データエクセルの単記票、テキスト読取フラグ
Public Const co_On = "1"

'マージテキストファイル名
Public Const co_MrgTxt = "調査票マージファイル"

'調査票番号の繰上りデータ数
Public Const co_CNoUp = 20

'デバッグ用項目間の区切り改行
Public g_Dlm As String                  '区切り改行(製品版は"")

'*************************************************************************
'エクセル定義
'*************************************************************************
Public Const co_MainSht = "電子調査票データ処理システム"    'ツールのメインシート名
Public Const co_KanriSht = "調査票管理シート"   'ツールの管理シート
Public Const co_MstSht = "マスタ"               'ツールのマスタシート

'*************************************************************************
'セル座標定義
'*************************************************************************
'ツールエクセルのメインシート
Public Const co_oCell = "A7"        '�@ 調査票テキストファイル　出力先フォルダ
Public Const co_m_iCell = "A19"     '�@ 調査票テキストファイル（マージ）　入力用フォルダ
Public Const co_m_oCell = "A24"     '�A 調査票テキストファイル（マージ）　出力先フォルダ

'ツールエクセルの管理シート
Public Const co_KStartRow = 15      'データ部スタート行
Public Const co_KCDTdCol = 1        'CDデータ内容 都道府県番号列
Public Const co_KCDTnCol = 2        'CDデータ内容 都道府県名列
Public Const co_KCDSsCol = 3        'CDデータ内容 施設番号列
Public Const co_KCDFnCol = 4        'CDデータ内容 ファイル名
Public Const co_KCDDcCol = 5        'CDデータ内容 データ件数
Public Const co_KISTdCol = 6        '医療施設区分 都道府県番号列
Public Const co_KISTnCol = 7        '医療施設区分 都道府県名列
Public Const co_KISSsCol = 8        '医療施設区分 施設番号列
Public Const co_KISCsCol = 9        '医療施設区分 調査票種別
Public Const co_KISCcCol = 10       '医療施設区分 調査票コード
Public Const co_KISCnCol = 11       '医療施設区分 調査票名
Public Const co_KSDTnCol = 12       '出力データ件数 単記票
Public Const co_KSDRnCol = 13       '出力データ件数 連記票
Public Const co_KSDTyCol = 14       '出力データ件数 テキスト読込有り
Public Const co_KSDDyCol = 15       '出力データ件数 DPC読込有り
Public Const co_KSDRyCol = 16       '出力データ件数 レセプト読込有り
Public Const co_KERSoCol = 17       'エラー区分 シート1個
Public Const co_KERTsCol = 18       'エラー区分 提出用
Public Const co_KERBsCol = 19       'エラー区分 番号相違
Public Const co_KERCtCol = 20       'エラー区分 調査票重複
Public Const co_KERLmCol = 21       'エラー区分 ラベル目視
Public Const co_KSYmdCol = 22       '処理日時

'ツールエクセルのマスタシート
Public Const co_MTdfkCell = "マスタ!A2:B48" '都道府県マスタ セル範囲
Public Const co_MTdfkSRow = 2               '都道府県マスタ データ開始行
Public Const co_MTdfkERow = 48              '都道府県マスタ データ終了行
Public Const co_MTdfkIDCol = 1              '都道府県マスタ ID列

Public Const co_MKbnCell = "マスタ!D2:D4"   '調査区分マスタ セル範囲

Public Const co_MCysSRow = 2                '調査票マスタ データ開始行
Public Const co_MCysERow = 10               '調査票マスタ データ終了行
Public Const co_MCysIDCol = 6               '調査票マスタ ID列

'データエクセル
Public Const co_DTdCell = "L1"      '都道府県番号
Public Const co_DCsCell = "M1"      '調査票種別
Public Const co_DCmCell = "N1"      '調査票コード
Public Const co_DSnCell = "L6"      '施設番号
Public Const co_DstartRow = 14      'データスタート行
Public Const co_DstartCol = 1       'データ開始列
Public Const co_DcheckCol = 2       'データ確認列(偶数票以外)
Public Const co_DcheckColG = 3      'データ確認列(偶数票のみ)

'*************************************************************************
'テキスト関係
'*************************************************************************
Public Const co_adTypeText = 2              'テキストタイプ
Public Const co_adSaveCreateOverWrite = 2   '保存形式
Public Const co_Chara_EUC = "EUC-JP"        '文字コード(EUC-JP)
Public Const co_LF = 10                     '改行コード(LF)
Public Const co_CR = 13                     '改行コード(CR)
Public Const co_CRLF = -1                   '改行コード(CRLF)
Public Const co_Ext_Txt = ".txt"            'テキスト拡張子


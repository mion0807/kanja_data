Attribute VB_Name = "MD_Common"
'*************************************************************************************
' 共通処理モジュール
'*************************************************************************************
Option Explicit

'*************************************************************************
'関数名　：FileList
'概要　　：再帰処理でファイル抽出
'引数　　：sFolder As String            '検索フォルダ
'          sFSO As FileSystemObject     'FSOオブジェクト
'          sFoundSubFolder As String    'サブフォルダ検索可否(TRUE:検索する、FALSE:検索しない)
'          sFileType As String          'ファイル拡張子
'          sFileList() As String        '検索結果ファイル格納用配列
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub FileList(sFolder As String, sFSO As FileSystemObject, sFoundSubFolder As String, sFileType As String, sFileList() As String)

    Dim oFolder As Object
    Dim o0 As Object
    Dim o1 As Object
    
    Dim dIchi As Integer
    Dim dLen As Integer
    Dim dFolder As String
    Dim dFile As String
    
    Dim strArray() As String
    Dim lLoop As Long
    
    '--------------------------------
    'チェック処理
    '--------------------------------
    If Not sFSO.FolderExists(sFolder) Then      'フォルダが存在しない場合は終了
        Exit Sub
    End If
    
    '--------------------------------
    'ファイル検索
    '--------------------------------
    '検索条件をセミコロンで分割
    strArray = Split(sFileType, ";")
    
    '検索処理
    Set oFolder = sFSO.GetFolder(sFolder)
    Set o0 = oFolder.Files
    For Each o1 In o0
    
        dLen = Len(o1.path)                     'フルパスの文字数をカウントする。
        dIchi = InStrRev(o1.path, "\")          '一番右にある"\"の位置を探す。
        dFolder = Left(o1.path, dIchi)          'フォルダ名（ファイル名を除く）
        dFile = Mid(o1.path, dIchi + 1)         'ファイル名
        
        For lLoop = 0 To UBound(strArray)
            If (dFile Like strArray(lLoop)) = True Then
                sFileList(UBound(sFileList)) = o1.path
                ReDim Preserve sFileList(UBound(sFileList) + 1)
            End If
        Next
    Next

    DoEvents
    
    '--------------------------------
    'サブフォルダ検索(再帰処理)
    '--------------------------------
    If UCase(sFoundSubFolder) = "TRUE" Then
        Set o0 = oFolder.SubFolders
        For Each o1 In o0
            If (o1.Attributes And (2 + 4)) = 0 Then
                FileList o1.path, sFSO, sFoundSubFolder, sFileType, sFileList
            End If
        Next
    End If
    
End Sub

'*************************************************************************
'関数名　：checkShtExist
'概要　　：シート存在チェック
'引数　　：tmpWB As Workbook    '存在チェックするエクセルワークブック
'          chkShtName As String '存在チェックするシート名
'戻り値　：Boolean  True:存在する、False:存在しない
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkShtExist(tmpWB As Workbook, chkShtName As String) As Boolean

    Dim tempSheet As Worksheet

    '--------------------------------
    '初期化
    '--------------------------------
    checkShtExist = False

    '--------------------------------
    'シート検索
    '--------------------------------
    For Each tempSheet In tmpWB.Sheets
        If LCase(chkShtName) = LCase(tempSheet.name) Then
            checkShtExist = True
            Exit For
        End If
    Next

End Function

'*************************************************************************
'関数名　：openCDTray
'概要　　：CDトレイオープン処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub openCDTray()

On Error GoTo ErrProc

    'CDトレイオープン
    #If Debug_Flg <> 1 Then
        mciSendStringA "Set CDAudio Door Open", 0&, 0, 0
    #End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：openCDTray " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：closeCDTray
'概要　　：CDトレイクローズ処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub closeCDTray()

On Error GoTo ErrProc

    'CDトレイクローズ
    #If Debug_Flg <> 1 Then
        mciSendStringA "Set CDAudio Door Closed", 0&, 0, 0
    #End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：closeCDTray " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：waitCDRead
'概要　　：CD情報読込待ち処理
'引数　　：strDrive as String   ドライブ
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub waitCDRead(strDrive As String)

'ドライブ情報
Dim fso         As New FileSystemObject 'FileSystemObject
Dim dInfo       As Object               'ドライブ情報

'時間変数
Dim vStartTime  As Variant  '処理開始時間
Dim vNowTime    As Variant  '現在時間
Dim lTimeLimit  As Long     '待ち時間制限(秒)

On Error GoTo ErrProc

    #If Debug_Flg <> 1 Then
        '初期化
        vStartTime = Timer              '開始時間をセット
        lTimeLimit = co_LimitTime       '制限をセット
        
        '制限時間がくるまでループ処理
        Do
            'ドライブ情報取得
            Set dInfo = fso.GetDrive(fso.GetDriveName(strDrive))
            
            'ドライブの準備が出来ている場合
            If dInfo.IsReady = True Then
                '終了処理
                GoTo FinProc
            End If
            
            '現在時間をセット
            vNowTime = Timer
            
        Loop While vNowTime - vStartTime < lTimeLimit
        
    #End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：waitCDRead " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    Set dInfo = Nothing
    Set fso = Nothing
    Exit Sub

End Sub

'*************************************************************************
'関数名　：getMaxRow
'概要　　：最大行取得処理
'引数　　：tmpSht As Worksheet  '確認するシートオブジェクト
'戻り値　：Long 最大行
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function getMaxRow(tmpSht As Worksheet) As Long

On Error GoTo ErrProc

    '--------------------------------
    '初期化
    '--------------------------------
    getMaxRow = 0

    '--------------------------------
    '最大行取得
    '--------------------------------
'    getMaxRow = tmpSht.Range("A1").SpecialCells(xlLastCell).Offset(1, 0).Row
    getMaxRow = tmpSht.Range("A65536").End(xlUp).Row + 1

    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：getMaxRow " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:

End Function

'*************************************************************************
'関数名　：getMstDataValue
'概要　　：マスタデータから名称部の取得
'引数　　：mstWST As Worksheet  チェックするマスタシート
'          strID As String      キーとなるID
'          strKbn As String     取得するマスタの区分
'                               1:都道府県マスタ、2:調査票マスタ
'戻り値　：String   名称
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function getMstDataValue(mstWST As Worksheet, strID As String, strKbn As String) As String

Dim lStartRow   As Long     'データ検索開始行
Dim lEndRow     As Long     'データ検索終了行
Dim lIDCol      As Long     'ID列
Dim lNameCol    As Long     '名称列
Dim lLoop       As Long     'ループ変数
Dim strRet      As String   'リターン値

    '--------------------------------
    '初期化
    '--------------------------------
    strRet = ""
    getMstDataValue = ""
    
    '取得するマスタの区分によって取得する座標が異なる
    Select Case strKbn
        '都道府県マスタ
        Case co_MstTdfk
            lStartRow = co_MTdfkSRow
            lEndRow = co_MTdfkERow
            lIDCol = co_MTdfkIDCol
            lNameCol = lIDCol + 1
            
        '調査票マスタ
        Case co_MstCysh
            lStartRow = co_MCysSRow
            lEndRow = co_MCysERow
            lIDCol = co_MCysIDCol
            lNameCol = lIDCol + 1
        
    End Select

    '--------------------------------
    '検索処理
    '--------------------------------
    For lLoop = lStartRow To lEndRow
        'IDが一致した場合
        If mstWST.Cells(lLoop, lIDCol) = strID Then
            'リターン値をセットしてループ終了
            strRet = mstWST.Cells(lLoop, lNameCol)
            Exit For
        End If
    Next

    '--------------------------------
    '終了処理
    '--------------------------------
    getMstDataValue = strRet
    
    
End Function

'*************************************************************************
'関数名　：padText
'概要　　：指定桁数を指定文字で埋める
'          ただし空白の場合は指定桁数を空白で埋める
'引数　　：strTmp As String     変換元文字列
'          strPad As String     埋める指定文字
'          lPad As Long         指定桁数
'          iSbt As Integer      種別(1:半角、2:全角)
'戻り値　：String 変換後文字列
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function padText(strTmp As String, strPad As String, lPad As Long, iSbt As Integer) As String

    Dim strSpace    As String   'スペース
    Dim strFormat   As String   'Format関数に用いる形式
    Dim lLoop       As Long     'ループ変数

    '--------------------------------
    '初期化
    '--------------------------------
    padText = ""
    strSpace = " "
    strFormat = ""
    
    '種別ごとの処理
    Select Case iSbt
        
        '半角の場合
        Case co_Han
            '空白の場合
            If IsNull(strTmp) Or strTmp = "" Then
                For lLoop = 1 To lPad
                    padText = padText & strSpace
                Next
            '空白以外の場合
            Else
                For lLoop = 1 To lPad
                    strFormat = strFormat & strPad
                Next
                padText = Format(strTmp, strFormat)
            End If
        
        '全角の場合
        Case co_Zen
            '半角があれば全角にする
            strTmp = StrConv(strTmp, vbWide)
            strSpace = StrConv(strSpace, vbWide)
        
            '後にスペースをつける
            For lLoop = Len(strTmp) + 1 To lPad
                strTmp = strTmp & strSpace
            Next
            
            'セットしてリターン
            padText = strTmp
            Exit Function
            
    End Select
    
End Function


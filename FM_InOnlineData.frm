VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FM_InOnlineData 
   Caption         =   "読取CD情報入力画面"
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8430
   OleObjectBlob   =   "FM_InOnlineData.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "FM_InOnlineData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit


Private Sub txtDirectoryBtn_Click()
'===================================================================
' 参照ボタン押下時の動作
' →ファイルを参照しパスをテキストボックスに表示
'===================================================================
'//ダイアログのタイトルを設定
    Dim Dialog_Title As String
    kaitoFilePath = ""
    Dialog_Title = "フォルダを選択して下さい"
'//ファイル参照ダイアログの表示
'    Dim filePath As Variant
    kaitoFilePath = GetDirectory(Dialog_Title)
'//ファイル参照ダイアログの表示
    If kaitoFilePath = False Then
        '//キャンセルされたら処理を終了します
        End
    Else
        '//ファイルが指定されたらファイルパスをセルに入力
        cmbDrive = kaitoFilePath
    End If
End Sub

'*************************************************************************
'関数名　：UserForm_Initialize
'概要　　：フォーム初期処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub UserForm_Initialize()

Dim fso         As New FileSystemObject 'FileSystemObject
Dim lLoop       As Long                 'ループ変数

'ドライブ情報
Dim dCollect    As Object               'ドライブコレクション
Dim dMember     As Object               'ドライブメンバ

On Error GoTo ErrProc

    'CD情報設定画面を開く
    With Me
        'ドライブコンボボックス初期化
'        .cmbDrive.Clear

        'ドライブコンボボックス値設定
        Set dCollect = fso.Drives
        For Each dMember In dCollect
            'アイテム追加
            #If Debug_Flg = 1 Then
                .cmbDrive.AddItem dMember.DriveLetter & ":\"
            #End If
            
'            'ドライブタイプがCDの場合 制限解除
'            If dMember.DriveType = co_CDType Then
                'アイテム追加
                #If Debug_Flg <> 1 Then
'                    .cmbDrive.AddItem dMember.DriveLetter & ":\"
                #End If
                
                'デフォルト選択
                .cmbDrive = dMember.DriveLetter & ":\"
'            End If
        Next
        
        '都道府県番号コンボボックス初期化
        .cmbTodofuken.Clear

        '都道府県番号ボボックス値設定
        .cmbTodofuken.ColumnCount = 2
        .cmbTodofuken.RowSource = co_MTdfkCell
        .cmbTodofuken.ListIndex = -1
        .cmbTodofuken.TextColumn = 2
        
        '施設番号コンボボックス初期化
'        .cmbKbn.Clear

        '施設番号ボボックス値設定
'        .cmbKbn.RowSource = co_MKbnCell
        
        '施設番号テキスト初期化
'        .txtShisetsu = ""
        
    End With
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：UserForm_Initialize " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    '解放
    Set dCollect = Nothing
    Set dMember = Nothing
    Set fso = Nothing
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnExe_Click
'概要　　：実行ボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnExe_Click()

Dim strMsg          As String

On Error GoTo ErrProc
    
    '--------------------------------
    '入力チェック
    '--------------------------------
    If checkInputData = False Then
        GoTo FinProc
    End If
    'CDトレイオープン
'    openCDTray
    
    '確認メッセージ
    strMsg = "テキスト出力処理を実行します。" & vbCrLf & "OKボタンを押して下さい。" 'CDをドライブにセットしてから
    If MsgBox(strMsg, vbOKCancel, "テキストファイル　出力確認") = vbOK Then
        'CDトレイクローズ
'        closeCDTray
    
        '実行中画面表示
        FM_Now.Show (vbModeless)
        DoEvents
        
        'CD内容読み込むまで待つ
        #If Develop_Flg = 0 Then
            Call waitCDRead(Me.cmbDrive)
        #End If
        
        'エクセルデータ確認処理
        If checkExcelData = True Then
            '画面を閉じる
            Unload Me
        End If
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnExe_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Exit Sub

End Sub

'*************************************************************************
'関数名　：btnCancel_Click
'概要　　：キャンセルボタン押下時
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Sub btnCancel_Click()

On Error GoTo ErrProc

    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc

'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：btnCancel_Click " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc

'finally
FinProc:
    Unload Me
    Exit Sub

End Sub

'*************************************************************************
'関数名　：checkInputData
'概要　　：入力チェック処理(テキスト出力時)
'引数　　：なし
'戻り値　：Boolean  True:入力チェックOK、False:入力チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Private Function checkInputData() As Boolean

Dim fso As New FileSystemObject 'FileSystemObject

On Error GoTo ErrProc

    '--------------------------------
    '初期化
    '--------------------------------
    checkInputData = False

    '--------------------------------
    '必須チェック
    '--------------------------------
    '読取ドライブコンボボックス
    If Me.cmbDrive = "" Then
        'エラーとして処理を終了
        MsgBox "読取ドライブが選択されていません。選択してください。", vbOKOnly + vbExclamation
        Me.cmbDrive.SetFocus
        GoTo FinProc
    End If
    
    '都道府県番号コンボボックス
    If Me.cmbTodofuken = "" Then
        'エラーとして処理を終了
        MsgBox "都道府県番号が選択されていません。選択してください。", vbOKOnly + vbExclamation
        Me.cmbTodofuken.SetFocus
        GoTo FinProc
    End If
    
'    '施設番号コンボボックス
'    If Me.cmbKbn = "" Then
'        'エラーとして処理を終了
'        MsgBox "施設番号の区分が選択されていません。選択してください。", vbOKOnly + vbExclamation
'        Me.cmbKbn.SetFocus
'        GoTo FinProc
'    End If
    
    '施設番号テキスト
'    If Me.txtShisetsu = "" Then
'        'エラーとして処理を終了
'        MsgBox "施設番号が入力されていません。入力してください。", vbOKOnly + vbExclamation
'        Me.txtShisetsu.SetFocus
'        GoTo FinProc
'    End If
    
    '--------------------------------
    '存在チェック
    '--------------------------------
    '読取ドライブ
'    If fso.DriveExists(Me.cmbDrive) = False Then
'        'エラーとして処理を終了
'        MsgBox "読取ドライブが存在しません。再度確認してください。", vbOKOnly + vbExclamation
'        GoTo FinProc
'    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    checkInputData = True
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：checkInputData " & "No." & Err.Number & " : " & Err.Description
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:
    '解放
    Set fso = Nothing

End Function

'*************************************************************************
'関数名　：checkExcelData
'概要　　：エクセルデータ確認処理
'引数　　：なし
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkExcelData() As Boolean

'ファイル検索系
Dim fso             As New FileSystemObject 'FileSystemObject
Dim sFolder         As String               '検索フォルダ名
Dim sFoundSubFolder As String               'サブフォルダ内のファイルを取得するか？ TRUE:全て取得 それ以外:フォルダのみ
Dim sFileList()     As String               '処理対象ファイルリスト
Dim sFileType       As String               '処理対象ファイル拡張子(セミコロン区切り)
Dim lLoop           As Long                 '処理対象ファイルのループ変数

'ツールファイル用変数
Dim orgWB           As Workbook             'ツールファイル
Dim lStartRow       As Long                 '管理シートデータ部開始行
Dim lRow            As Long                 '管理シートデータ部処理カレント行

'入力元ファイル用変数
Dim tmpWB           As Workbook             '各エクセルファイル
Dim dataCnt         As Long                 'データ件数

'その他処理変数
Dim strMsg          As String
Dim iLoop           As Integer              'ループ変数
Dim errFlg          As Boolean              'エラーフラグ True:エラーあり False:エラー無し

On Error GoTo ErrProc

    '初期化
    errFlg = False
    dataCnt = 0
    checkExcelData = False

    '非表示にセット(メッセージ、画面)
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    
    'フォルダ配下(サブフォルダ含む)のxlsファイルを全て処理する
    sFolder = Me.cmbDrive           '検索ルートフォルダ
    
    sFileType = "*.txt"             '検索ファイル名(複数ある場合はセミコロンで区切る)txtのみにする？
    sFoundSubFolder = "TRUE"        'サブフォルダは検索対象
    ReDim sFileList(0)
    
    'WorkBook変数の指定
    Set orgWB = ActiveWorkbook  'ツールのエクセルオブジェクト指定
    
    '対象ファイルを配列に格納
    Call FileList(sFolder, fso, sFoundSubFolder, sFileType, sFileList)
    
    '処理対象ファイルが存在しない場合は処理中断
    If UBound(sFileList) = 0 Then
        '実行中画面閉じる
        Application.ScreenUpdating = True
        Unload FM_Now
        DoEvents
        Application.ScreenUpdating = False
        
        'メッセージ
        strMsg = "処理対象ファイルが存在しないため処理を中断します。" & vbCrLf & "再度CDをご確認下さい。"
        MsgBox strMsg, vbOKOnly, "処理中断"
        GoTo FinProc
    End If
    
    '管理シートデータ部の最大行を取得
    lStartRow = getMaxRow(orgWB.Sheets(co_KanriSht))
    lRow = lStartRow
    
    '対象ファイルを一つずつ処理
    For lLoop = 0 To UBound(sFileList) - 1
    
        If InStrRev(sFileList(lLoop), ".xlsm") Then
       
            '管理シートデータ初期化
            With orgWB.Sheets(co_KanriSht)
                For iLoop = co_KCDTdCol To co_KSYmdCol
                    .Cells(lRow, iLoop) = co_Haifun
                Next
            End With
        
            'エクセルファイルを開く
            Application.EnableEvents = False    'マクロ無効
            Workbooks.Open FileName:=sFileList(lLoop), ReadOnly:=True
            Application.EnableEvents = True     'マクロ有効
            Set tmpWB = ActiveWorkbook
    
            'シート1個チェック
            If checkShtCnt(tmpWB) = True Then
                '○印
                orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERSoCol) = co_Maru
                
                '提出用チェック
                If checkTeisyutsu(tmpWB) = True Then
                    '○印
                    orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERTsCol) = co_Maru
                    
                    '番号相違チェック
                    If checkSoui(tmpWB) = True Then
                        '管理シートにデータ記載
                        With orgWB.Sheets(co_KanriSht)
                            .Cells(lRow, co_KERBsCol) = co_Maru            '○印
                            .Cells(lRow, co_KCDDcCol) = getDataCnt(tmpWB)  'データ件数
                            .Cells(lRow, co_KISTdCol) = "'" & padText(tmpWB.Sheets(1).Range(co_DTdCell), co_Zr, 2, co_Han)  '都道府県番号"
                            .Cells(lRow, co_KISTnCol) = getMstDataValue(orgWB.Sheets(co_MstSht) _
                                                            , padText(tmpWB.Sheets(1).Range(co_DTdCell), co_Zr, 2, co_Han) _
                                                            , co_MstTdfk)                                                   '都道府県名
                            .Cells(lRow, co_KISSsCol) = Left(tmpWB.Sheets(1).Range(co_DCmCell), 1) _
                                                            & co_Haifun _
                                                            & tmpWB.Sheets(1).Range(co_DSnCell)                             '施設番号
                            
                            If tmpWB.Sheets(1).Range(co_DCmCell) = co_HGN Then
                                .Cells(lRow, co_KISCsCol) = "30"                                                            '調査票種別
                            ElseIf tmpWB.Sheets(1).Range(co_DCmCell) = co_HGG Then
                                .Cells(lRow, co_KISCsCol) = "31"                                                            '調査票種別
                            Else
                                .Cells(lRow, co_KISCsCol) = tmpWB.Sheets(1).Range(co_DCsCell) & "0"                         '調査票種別
                            End If

                            .Cells(lRow, co_KISCcCol) = tmpWB.Sheets(1).Range(co_DCmCell)                                   '調査票コード
                            .Cells(lRow, co_KISCnCol) = getMstDataValue(orgWB.Sheets(co_MstSht) _
                                                            , tmpWB.Sheets(1).Range(co_DCmCell) _
                                                            , co_MstCysh)                                                   '調査票名
                        End With
                    Else
                        '×印
                        orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERBsCol) = co_Batsu
                        errFlg = True
                    End If
                Else
                    '×印
                    orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERTsCol) = co_Batsu
                    errFlg = True
                End If
            Else
                '×印
                orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERSoCol) = co_Batsu
                errFlg = True
            End If
    
            'エクセルファイルを閉じる
            tmpWB.Close SaveChanges:=False
    
            '解放
            Set tmpWB = Nothing
    
            '管理シートにデータ記載
            With orgWB.Sheets(co_KanriSht)
                .Cells(lRow, co_KCDTdCol) = Me.cmbTodofuken                         'CDデータ内容 都道府県番号
                .Cells(lRow, co_KCDTnCol) = Me.cmbTodofuken.Text                    'CDデータ内容 都道府県名
                .Cells(lRow, co_KCDSsCol) = Left(Me.cmbDrive, 1) & co_Haifun & "999"          'CDデータ内容 施設番号
                .Cells(lRow, co_KCDFnCol) = sFileList(lLoop)                              'CDデータ内容 ファイル名
                .Cells(lRow, co_KSYmdCol) = Format(Now, "YYYY/MM/DD HH:NN:SS")      '処理日時
            End With
            
            '書式設定
            With orgWB.Sheets(co_KanriSht).Range( _
                orgWB.Sheets(co_KanriSht).Cells(lRow, co_KCDTdCol) _
                , orgWB.Sheets(co_KanriSht).Cells(lRow, co_KSYmdCol))
                    .Borders.LineStyle = xlContinuous   '罫線を引く
                    .WrapText = True                    '折り返して表示
                    .Font.name = "ＭＳ Ｐゴシック"      'フォントタイプ
                    .Font.Size = 10                     'フォントサイズ
            End With
            
            '行のカウントアップ
            lRow = lRow + 1
    
        ElseIf InStrRev(sFileList(lLoop), ".txt") Then
            '管理シートにデータ記載 オンライン版
            With orgWB.Sheets(co_KanriSht)
                .Cells(lRow, co_KCDTdCol) = Me.cmbTodofuken                         'CDデータ内容 都道府県番号
                .Cells(lRow, co_KCDTnCol) = Me.cmbTodofuken.Text                    'CDデータ内容 都道府県名
                .Cells(lRow, co_KCDSsCol) = Left(Me.cmbDrive, 1) & co_Haifun & "999"            'CDデータ内容 施設番号
                .Cells(lRow, co_KCDFnCol) = sFileList(lLoop)                        'CDデータ内容 ファイル名
                .Cells(lRow, co_KSYmdCol) = Format(Now, "YYYY/MM/DD HH:NN:SS")      '処理日時
            End With
            
            '書式設定
            With orgWB.Sheets(co_KanriSht).Range( _
                orgWB.Sheets(co_KanriSht).Cells(lRow, co_KCDTdCol) _
                , orgWB.Sheets(co_KanriSht).Cells(lRow, co_KSYmdCol))
                    .Borders.LineStyle = xlContinuous   '罫線を引く
                    .WrapText = True                    '折り返して表示
                    .Font.name = "ＭＳ Ｐゴシック"      'フォントタイプ
                    .Font.Size = 10                     'フォントサイズ
            End With
            
            '番号相違チェック
            If checkText(sFileList(lLoop)) = False Then
                '×印
                orgWB.Sheets(co_KanriSht).Cells(lRow, co_KERBsCol) = co_Batsu
                errFlg = True
            
            End If
            
            
            '行のカウントアップ
            lRow = lRow + 1
        End If
    Next
    
    '行を1行戻す(最大行+1まで進んでしまっているため)
    lRow = lRow - 1
    
    checkExcelData = True
    
    'エラーが無い場合
    If errFlg = False Then
        '調査票重複を確認
        If checkCyofuku(orgWB.Sheets(co_KanriSht), lStartRow, lRow) = True Then
            '実行中画面閉じる
            Application.ScreenUpdating = True
            Unload FM_Now
            DoEvents
            Application.ScreenUpdating = False
        
            'エラーが無ければ目視確認画面を表示
            Call setMokushiData(orgWB.Sheets(co_KanriSht), lStartRow, lRow)
        
        Else
            '実行中画面閉じる
            Application.ScreenUpdating = True
            Unload FM_Now
            DoEvents
            Application.ScreenUpdating = False
            
            'CDトレイオープン
            openCDTray
            
            'メッセージ
            strMsg = "形式エラーのため処理を中断します。" & vbCrLf & "エラー詳細は" & co_KanriSht & "をご確認下さい。"
            MsgBox strMsg, vbOKOnly, "処理中断"
            
        End If
    Else
        '実行中画面閉じる
        Application.ScreenUpdating = True
        Unload FM_Now
        DoEvents
        Application.ScreenUpdating = False
        
        'CDトレイオープン
        openCDTray
        
        'メッセージ
        strMsg = "形式エラーのため処理を中断します。" & vbCrLf & "エラー詳細は" & co_KanriSht & "をご確認下さい。"
        MsgBox strMsg, vbOKOnly, "処理中断"
        
    End If
    
    '--------------------------------
    '終了処理
    '--------------------------------
    GoTo FinProc
    
'catch
ErrProc:
    'エラー処理
    MsgBox "関数名：checkExcelData " & "No." & Err.Number & " : " & Err.Description
    Err.Raise "999"
    
    '終了処理
    GoTo FinProc
    
'finally
FinProc:
    '解放
    Set fso = Nothing
    If Not (tmpWB Is Nothing) Then
        tmpWB.Close
    End If
    Set tmpWB = Nothing
    Set orgWB = Nothing
    
    '表示にセット(メッセージ、画面)
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True

End Function

'*************************************************************************
'関数名　：checkShtCnt
'概要　　：シート数チェック
'引数　　：tmpWB As Workbook    'チェックするエクセルワークブック
'戻り値　：Boolean  True:チェックOK、False:チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkShtCnt(tmpWB As Workbook) As Boolean

    '--------------------------------
    '初期化
    '--------------------------------
    checkShtCnt = False

    '--------------------------------
    'シート個数確認
    '--------------------------------
    If tmpWB.Sheets.Count = 1 Then
        checkShtCnt = True
    End If

End Function

'*************************************************************************
'関数名　：checkTeisyutsu
'概要　　：提出用チェック
'引数　　：tmpWB As Workbook    'チェックするエクセルワークブック
'戻り値　：Boolean  True:チェックOK、False:チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkTeisyutsu(tmpWB As Workbook) As Boolean

    '--------------------------------
    '初期化
    '--------------------------------
    checkTeisyutsu = False

    '--------------------------------
    '値が存在するか確認
    '--------------------------------
    If Trim(tmpWB.Sheets(1).Range(co_DTdCell)) <> "" _
        And Trim(tmpWB.Sheets(1).Range(co_DCsCell)) <> "" _
        And Trim(tmpWB.Sheets(1).Range(co_DCmCell)) <> "" _
        And Trim(tmpWB.Sheets(1).Range(co_DSnCell)) <> "" Then
            checkTeisyutsu = True
    End If

End Function

'*************************************************************************
'関数名　：checkSoui
'概要　　：番号相違チェック
'引数　　：tmpWB As Workbook    'チェックするエクセルワークブック
'戻り値　：Boolean  True:チェックOK、False:チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkSoui(tmpWB As Workbook) As Boolean

    '--------------------------------
    '初期化
    '--------------------------------
    checkSoui = False

    '--------------------------------
    '番号相違かどうか確認
    '--------------------------------
    '都道府県番号、調査票区分、施設番号が全て一致するか確認する
    If padText(Trim(tmpWB.Sheets(1).Range(co_DTdCell)), co_Zr, 2, co_Han) = Me.cmbTodofuken _
        And Left(Trim(tmpWB.Sheets(1).Range(co_DCmCell)), 1) = Me.cmbKbn _
        And Trim(tmpWB.Sheets(1).Range(co_DSnCell)) = Me.txtShisetsu Then
            checkSoui = True
    End If

End Function

'*************************************************************************
'関数名　：getDataCnt
'概要　　：データ件数取得処理
'引数　　：tmpWB As Workbook    'チェックするエクセルワークブック
'戻り値　：Long データ件数
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function getDataCnt(tmpWB As Workbook) As Long

Dim lLoop       As Long 'ループ変数
Dim lDataCnt    As Long 'データ件数
Dim checkCol    As Long 'データ確認列

    '--------------------------------
    '初期化
    '--------------------------------
    lLoop = co_DstartRow
    lDataCnt = 0
    getDataCnt = 0
    
    '偶数票の場合
    If tmpWB.Sheets(1).Range(co_DCmCell) = co_HGN Or tmpWB.Sheets(1).Range(co_DCmCell) = co_HGG Then
        'データ確認列が異なる
        checkCol = co_DcheckColG
    Else
        checkCol = co_DcheckCol
    End If

    '--------------------------------
    'データ件数取得
    '--------------------------------
    Do While tmpWB.Sheets(1).Cells(lLoop, checkCol) <> ""
        'カウントアップ
        lLoop = lLoop + 1
        lDataCnt = lDataCnt + 1
    Loop

    '--------------------------------
    '終了処理
    '--------------------------------
    getDataCnt = lDataCnt


End Function

'*************************************************************************
'関数名　：checkCyofuku
'概要　　：調査票重複チェック
'引数　　：kanriWST As Worksheet    チェックする管理シート
'          lStartRow as Long        チェック開始行
'          lEndRow as Long          チェック終了行
'戻り値　：Boolean  True:チェックOK、False:チェックNG
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Function checkCyofuku(kanriWST As Worksheet, lStartRow As Long, lEndRow As Long) As Boolean

Dim lLoop       As Long 'ループ変数
Dim lCLoop      As Long 'ループ変数

    '--------------------------------
    '初期化
    '--------------------------------
    lLoop = 0
    lCLoop = 0
    checkCyofuku = True

    '--------------------------------
    '調査票重複かどうか確認
    '--------------------------------
    For lLoop = lStartRow To lEndRow
        For lCLoop = lLoop + 1 To lEndRow
            If kanriWST.Cells(lLoop, co_KISCcCol) = kanriWST.Cells(lCLoop, co_KISCcCol) Then
                '×印
                kanriWST.Cells(lLoop, co_KERCtCol) = co_Batsu
                kanriWST.Cells(lCLoop, co_KERCtCol) = co_Batsu
                checkCyofuku = False
            Else
                If kanriWST.Cells(lLoop, co_KERCtCol) = co_Haifun Then
                    '○印
                    kanriWST.Cells(lLoop, co_KERCtCol) = co_Maru
                End If
            End If
        Next
        
        '一番最後のセルの印つけ
        If lLoop = lEndRow And kanriWST.Cells(lLoop, co_KERCtCol) = co_Haifun Then
            '○印
            kanriWST.Cells(lLoop, co_KERCtCol) = co_Maru
        End If
    Next
    
End Function

'*************************************************************************
'関数名　：setMokushiData
'概要　　：目視画面にデータセット
'引数　　：kanriWST As Worksheet    管理シート
'          lStartRow as Long        開始行
'          lEndRow as Long          終了行
'戻り値　：なし
'作成日　：2011/06/01
'更新日　：2011/06/01
'*************************************************************************
Public Sub setMokushiData(kanriWST As Worksheet, lStartRow As Long, lEndRow As Long)

Dim lLoop       As Long 'ループ変数

    '--------------------------------
    '初期化
    '--------------------------------
    lLoop = 0

    '--------------------------------
    'データセット
    '--------------------------------
    With FM_OnlineTextOutput
        .lblTodofuken.Caption = Me.cmbTodofuken.Text                    '都道府県名
        .lblShisetsu.Caption = Left(Me.cmbDrive, 1) & co_Haifun & "999"  '施設番号
        
        '全データの件数を確認して画面にセット
        For lLoop = lStartRow To lEndRow
            Select Case kanriWST.Cells(lLoop, co_KISCcCol)
            
                '病院入院（奇数）票
                Case co_HKN
                    .lblHKN.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '病院外来（奇数）票
                Case co_HKG
                    .lblHKG.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '病院入院（偶数）票
                Case co_HGN
                    .lblHGN.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '病院外来（偶数）票
                Case co_HGG
                    .lblHGG.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '病院退院票
                Case co_HT
                    .lblHT.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '一般診療所票（入院）
                Case co_CN
                    .lblCN.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '一般診療所票（外来）
                Case co_CG
                    .lblCG.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '一般診療所退院票
                Case co_CT
                    .lblCT.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
                '歯科診療所票
                Case co_D
                    .lblD.Caption = kanriWST.Cells(lLoop, co_KCDDcCol)
                    
            End Select
        Next
        
        '値を引き継ぐ 後、目視を表示せずに処理を行う
        Call .frmInit(lStartRow, lEndRow, Me.cmbDrive)
        
        '表示　上記の改修により削除
'        .Show (vbModeless)
        
        'CDトレイオープン
        'H26 指摘により削除
        'openCDTray
        
    End With
    
End Sub
'*************************************************************************
'関数名　：checkText
'概要　　：テキストチェック
'*************************************************************************
Public Function checkText(FileName As String) As Boolean
    Dim intFF As Integer            ' FreeFile値
    Dim lngLOF As Long              ' LOF値
    Dim lngPOS As Long              ' 読み込み位置
    Dim HeaderBuf As String
    Dim chosahyo_id As String
    
    
    
    
    HeaderBuf = Space(85)

    '--------------------------------
    '初期化
    '--------------------------------
    checkText = False
    intFF = FreeFile
   ' 指定ファイルをOPEN(入力モード)
    Open FileName For Binary As #intFF
    lngLOF = LOF(intFF)             ' LOF値(ファイルサイズ)取得
    lngPOS = 1                      ' 読み込み位置
    
    
    Get #intFF, lngPOS, HeaderBuf
        
    chosahyo_id = Trim(Mid(HeaderBuf, 9, 12))
    
'    Opne FileName For Input As #1
'        Line Input #1, chosahyo_id
'    Close #1

    
    If chosahyo_id = taishosha_H Or _
        chosahyo_id = taishosha_C Or _
            chosahyo_id = taishosha_D Then
        checkText = True
    End If
  
    ' 指定ファイルをCLOSE
    Close #intFF

End Function



VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
'保存イベント前に実行される処理
Private Sub Workbook_BeforeSave(ByVal SaveAsUI As Boolean, Cancel As Boolean)
    Dim rt As Integer
    'SetPartProtect False, mySheets.getEditableSheet
    #If (Develop_Flg) Then
        rt = MsgBox("ソースを保存しますか？", vbYesNo + vbQuestion, "確認")
        If rt = vbYes Then
            Call vbaComponentExport
        End If
        
        'VBAプロジェクトのソース一式を出力
    #End If
End Sub



